const httpUrl = 'http://localhost:8080/api/v1/game';

const ROOM_ID = 'ROOM_ID';
const USER_ID = 'USER_ID';
const MARKER_ID = 'MARKER_ID';
const LAST_UPDATE_ID = 'LAST_UPDATE_ID';

let createGameButton;

window.onload = function () {
    createGameButton = document.getElementById('create-game-button');

    configButton();
}

function configButton() {
    if (createGameButton) {
        createGameButton.addEventListener('click', createGame);
    }
}

function createGame() {
    removeCookie(ROOM_ID);
    removeCookie(USER_ID);
    removeCookie(MARKER_ID);
    removeCookie(LAST_UPDATE_ID)

    const httpClient = postNewHttpClient();

    const firstPlayerName = document.getElementById('first-player-name').value;
    const firstPlayerMarker = document.getElementById('first-player-marker').value;
    const secondPlayerName = document.getElementById('second-player-name').value;
    const secondPlayerMarker = document.getElementById('second-player-marker').value;

    const request = JSON.stringify(
        {
            "firstPlayer": {
                "name": firstPlayerName,
                "markerType": firstPlayerMarker
            },
            "secondPlayer": {
                "name": secondPlayerName,
                "markerType": secondPlayerMarker
            }
        });

    httpClient.send(request);

    let json;
    if (httpClient.readyState === 4 && httpClient.status === 200) {
        json = JSON.parse(httpClient.responseText);
        console.log(json);
        window.location.replace(json.link);
    }
}

function postNewHttpClient() {
    const httpClient = new XMLHttpRequest();

    httpClient.open("POST", httpUrl, false)
    httpClient.setRequestHeader("Content-Type", "application/json");
    httpClient.setRequestHeader("Access-Control-Allow-Origin", "*");
    httpClient.setRequestHeader("Access-Control-Allow-Methods", "DELETE, POST, GET, OPTIONS");
    httpClient.setRequestHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
    httpClient.setRequestHeader('Access-Control-Allow-Credentials', 'true');

    return httpClient;
}

function removeCookie(name) {
    return localStorage.removeItem(name);
}

const httpUrl = 'http://localhost:8080/api/v1/game';

const ROOM_ID = 'ROOM_ID';
const USER_ID = 'USER_ID';
const MARKER_ID = 'MARKER_ID';
const LAST_UPDATE_ID = 'LAST_UPDATE_ID';

let leaveGameButton;

let linksField;
let invitationLinkField_1;

let htmlField;

let fieldCSS;

let fieldSize;
let fieldCoordinatesXY;

let nexMove = false;

let roomData = {
    id: undefined,
    status: undefined,
    yourPlayer: {
        id: undefined,
        name: undefined,
        status: undefined,
        markerType: undefined
    },
    enemyPlayer: {
        id: undefined,
        name: undefined,
        status: undefined,
        markerType: undefined
    },
    link: {
        invitationLink: undefined
    },
    process: {
        field: {
            cells: undefined
        },
        lastMoveDate: undefined,
        lastRecordId: undefined,
        nextMovePlayerId: undefined
    }
};

let field = [[]];

window.onload = function () {
    leaveGameButton = document.getElementById('leave-game-button');
    linksField = document.getElementById('links');
    htmlField = document.getElementById('field');
    fieldCSS = document.getElementsByClassName('wrapper');

    configButton();
    initBeforeGame();
    joinGame();
    postInitGame();
}

function configButton() {
    if (leaveGameButton) {
        leaveGameButton.addEventListener('click', leaveGame);
        leaveGameButton.style.marginBottom = "100";
    }
}

function initBeforeGame() {
    if (invitationLinkField_1) {
        invitationLinkField_1.remove();
    }
    while (htmlField.firstChild) {
        htmlField.removeChild(htmlField.firstChild);
    }

    removeCookie(ROOM_ID);
    removeCookie(USER_ID);
    removeCookie(MARKER_ID);
    removeCookie(LAST_UPDATE_ID);
}

function joinGame() {
    const queryParams = parse_query_string(window.location.search.substring(1));
    const roomId = queryParams.roomId;
    const playerId = queryParams.playerId;

    console.log(httpUrl + "/test/" + roomId + "/" + playerId);

    if (roomId && playerId) {
        const httpClient = postNewHttpClient("/join/" + roomId + "/" + playerId);

        httpClient.send();

        if (httpClient.readyState === 4 && httpClient.status === 200) {
            roomData = JSON.parse(httpClient.responseText);
            console.log(roomData);

            setCookie(ROOM_ID, roomId);
            setCookie(USER_ID, playerId);
            setCookie(MARKER_ID, roomData.yourPlayer.markerType)

            initData();
            if (roomData.links && roomData.links.invitationLink) {
                addLinks(roomData.links);
            } else {
                addEmptyLinks();
            }
            drawCells(roomData.process.field.cells);
            // window.location.replace(json.yourLink);
        }
    }
}

function leaveGame() {
}

function addLinks(links) {
    let outputInvitationLinkField = document.createElement('input');
    outputInvitationLinkField.className = "head output";
    outputInvitationLinkField.size = "16";
    outputInvitationLinkField.style.width = (links.invitationLink.length * 8).toString();
    outputInvitationLinkField.value = links.invitationLink;
    outputInvitationLinkField.style.marginTop = "50";

    const invitationLinkField = document.createElement('div');
    invitationLinkField.className = "head h2";
    invitationLinkField.append(outputInvitationLinkField);

    linksField.append(invitationLinkField);
}

//Crutch - has to fixed
function addEmptyLinks() {
    let labelInvitationLinkField = document.createElement('label');
    labelInvitationLinkField.size = "16";
    labelInvitationLinkField.innerHTML = "joined!";
    labelInvitationLinkField.style.marginTop = "50";

    const invitationLinkField2 = document.createElement('div');
    invitationLinkField2.className = "head h2";
    invitationLinkField2.append(labelInvitationLinkField);

    linksField.appendChild(invitationLinkField2);
}

function initData() {
    fieldSize = roomData.process.field.cells.length;

    htmlField.style.width = ((fieldSize + 1) * 55).toString();
    htmlField.style.height = ((fieldSize + 1) * 55).toString();

    fieldCoordinatesXY = [];

    nexMove = getCookie(USER_ID) === roomData.process.nextMovePlayerId;
}

function postInitGame() {
    htmlField.scrollIntoView({
        behavior: 'smooth',
        block: 'start'
    });
}

function drawCells(cells) {
    field = cells;
    for (let y = 0; y < fieldSize; y++) {
        for (let x = 0; x < fieldSize; x++) {
            let cell = document.createElement('div');
            if (roomData.status === "PROCESS") {
                if (getCookie(USER_ID) === roomData.process.nextMovePlayerId) {
                    cell.className = "cell_move";
                } else {
                    cell.className = "cell_wait_move";
                }
            } else {
                cell.className = "cell_wait_player";
            }
            cell.id = x + '.' + y;
            const value = field[y][x];
            if (value !== "N") {
                cell.innerHTML = value;
            }
            htmlField.append(cell);
        }
    }
}

function postNewHttpClient(params) {
    const httpClient = new XMLHttpRequest();

    httpClient.open("POST", httpUrl + params, false)
    httpClient.setRequestHeader("Content-Type", "application/json");
    httpClient.setRequestHeader("Access-Control-Allow-Origin", "*");
    httpClient.setRequestHeader("Access-Control-Allow-Methods", "DELETE, POST, GET, OPTIONS");
    httpClient.setRequestHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
    httpClient.setRequestHeader('Access-Control-Allow-Credentials', 'true');

    return httpClient;
}

function getNewHttpClient(params) {
    const httpClient = new XMLHttpRequest();

    httpClient.open("GET", httpUrl + params, false)
    httpClient.setRequestHeader("Content-Type", "application/json");
    httpClient.setRequestHeader("Access-Control-Allow-Origin", "*");
    httpClient.setRequestHeader("Access-Control-Allow-Methods", "DELETE, POST, GET, OPTIONS");
    httpClient.setRequestHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
    httpClient.setRequestHeader('Access-Control-Allow-Credentials', 'true');

    return httpClient;
}

function setCookie(name, value) {
    // const date = new Date();
    // date.setTime(date.getTime() + (COOKIE_EXPIRED_DAYS * 24 * 60 * 60 * 1000));
    // const expires = "; expires=" + date.toUTCString();
    // document.cookie = name + "=" + (value || "") + expires + "; path=/";
    localStorage.setItem(name, value);
}

function getCookie(name) {
    return localStorage.getItem(name);
    // const nameEQ = name + "=";
    // const ca = document.cookie.split(';');
    // for (let i = 0; i < ca.length; i++) {
    //     let c = ca[i];
    //     while (c.charAt(0) === ' ') c = c.substring(1, c.length);
    //     if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
    // }
    // return null;
}

function removeCookie(name) {
    return localStorage.removeItem(name);
    // const nameEQ = name + "=";
    // const ca = document.cookie.split(';');
    // for (let i = 0; i < ca.length; i++) {
    //     let c = ca[i];
    //     while (c.charAt(0) === ' ') c = c.substring(1, c.length);
    //     if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
    // }
    // return null;
}

function eraseCookie(name) {
    document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function parse_query_string(query) {
    const vars = query.split("&");
    const query_string = {
        roomId: undefined,
        playerId: undefined
    };
    for (let i = 0; i < vars.length; i++) {
        const pair = vars[i].split("=");
        const key = decodeURIComponent(pair[0]);
        const value = decodeURIComponent(pair[1]);
        // If first entry with this name
        if (typeof query_string[key] === "undefined") {
            query_string[key] = decodeURIComponent(value);
            // If second entry with this name
        } else if (typeof query_string[key] === "string") {
            query_string[key] = [query_string[key], decodeURIComponent(value)];
            // If third or later entry with this name
        } else {
            query_string[key].push(decodeURIComponent(value));
        }
    }
    return query_string;
}

function getRoomData() {
    return roomData;
}

function getNextMove() {
    return nexMove;
}

function setNextMove(value) {
    nexMove = value;
}

function getCell(x, y) {
    return document.getElementById(x + '.' + y);
}

function setCellsMove() {
    setCellsStyle("cell_move");
}

function setCellsWait() {
    setCellsStyle("cell_wait_move");
}

function setCellsWin() {
    setCellsStyle("cell_win");
}

function setCellsLose() {
    setCellsStyle("cell_lose");
}

function setCellsDraw() {
    setCellsStyle("cell_draw");
}

function setCellsStyle(className) {
    for (let y = 0; y < fieldSize; y++) {
        for (let x = 0; x < fieldSize; x++) {
            getCell(x, y).className = className;
        }
    }
}

//
// export function dynamicallyLoadScriptProcess() {
//     const script = document.createElement("script");
//     script.src = "process.js";
//     document.head.appendChild(script);
// }
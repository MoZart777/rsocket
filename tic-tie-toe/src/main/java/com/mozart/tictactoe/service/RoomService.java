package com.mozart.tictactoe.service;

import com.mozart.tictactoe.helper.GameCreateHelper;
import com.mozart.tictactoe.helper.UpdateRomUpdateHelper;
import com.mozart.tictactoe.mapper.ApiMapperService;
import com.mozart.tictactoe.mapper.RepositoryMapperService;
import com.mozart.tictactoe.model.enums.CellMarker;
import com.mozart.tictactoe.model.enums.MarkerType;
import com.mozart.tictactoe.model.enums.PlayerStatus;
import com.mozart.tictactoe.model.enums.RoomStatus;
import com.mozart.tictactoe.model.redis.GameUpdate;
import com.mozart.tictactoe.model.request.MakeMoveRequest;
import com.mozart.tictactoe.model.request.RoomRequest;
import com.mozart.tictactoe.model.response.MessageResponse;
import com.mozart.tictactoe.model.response.RoomInitResponse;
import com.mozart.tictactoe.model.response.RoomResponse;
import com.mozart.tictactoe.persistance.entity.PlayerHE;
import com.mozart.tictactoe.persistance.entity.ProcessHE;
import com.mozart.tictactoe.persistance.entity.RoomHE;
import com.mozart.tictactoe.persistance.repository.RoomRepository;
import com.mozart.tictactoe.service.stream.GameStreamService;
import java.time.Instant;
import java.util.EnumMap;
import java.util.Map;
import java.util.function.Predicate;
import javax.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.stream.RecordId;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public final class RoomService {

    private final Map<MarkerType, CellMarker> markerMap = new EnumMap<>(MarkerType.class);

    private final RoomRepository roomRepository;
    private final GameStreamService streamReceiver;

    private final ApiMapperService apiMapperService;
    private final RepositoryMapperService repositoryMapperService;
    private final ResultService resultService;

    private final GameCreateHelper gameCreateHelper;
    private final UpdateRomUpdateHelper updateRomUpdateHelper;

    public RoomService(
            RoomRepository roomRepository,
            GameStreamService streamReceiver,
            ApiMapperService apiMapperService,
            RepositoryMapperService repositoryMapperService,
            ResultService resultService,
            GameCreateHelper gameCreateHelper,
            UpdateRomUpdateHelper updateRomUpdateHelper
    ) {
        this.roomRepository = roomRepository;
        this.streamReceiver = streamReceiver;

        this.apiMapperService = apiMapperService;
        this.repositoryMapperService = repositoryMapperService;
        this.resultService = resultService;

        this.gameCreateHelper = gameCreateHelper;
        this.updateRomUpdateHelper = updateRomUpdateHelper;

        markerMap.put(MarkerType.X, CellMarker.X);
        markerMap.put(MarkerType.O, CellMarker.O);
    }

    public Mono<RoomInitResponse> createNewGame(@NotNull RoomRequest request) {
        return Mono.justOrEmpty(request)
                .map(gameCreateHelper::createNewRoom)
                .doOnNext(roomRepository::save)
                .map(repositoryMapperService::mapToModel)
                .doOnNext(room -> log.debug("New room {} was created by {}", room.getId(), room.getFirstPlayer()))
                .map(apiMapperService::mapToRoomInitResponse);
    }

    public Mono<RoomResponse> getRoom(@NotNull String roomId, @NotNull String playerId) {
        return findById(roomId)
                .map(repositoryMapperService::mapToModel)
                .map(room -> apiMapperService.mapToRoomResponse(room, playerId));
    }

    public Mono<RoomResponse> joinRoom(@NotNull String roomId, @NotNull String playerId) {
        return findById(roomId)
                .doOnNext(room -> updateRoomIfNeed(room, playerId))
                .flatMap(room -> produceAndSaveUpdate(updateRomUpdateHelper.createJoinRoomUpdate(room, playerId), room)
                        .map(any -> apiMapperService.mapToRoomResponse(repositoryMapperService.mapToModel(room), playerId))
                );
    }

    public Mono<RoomResponse> leftRoom(@NotNull String roomId, @NotNull String playerId) {
        return findById(roomId)
                .doOnNext(room -> updateRoomIfNeed(room, playerId))
                .flatMap(room -> produceAndSaveUpdate(updateRomUpdateHelper.createPlayerLeftRoomUpdate(room), room)
                        .map(any -> apiMapperService.mapToRoomResponse(repositoryMapperService.mapToModel(room), playerId))
                );
    }

    public Mono<RoomResponse> testUpdate(@NotNull String roomId, @NotNull String playerId, String description) {
        return findById(roomId)
                .flatMap(room ->
                        produceAndSaveUpdate(
                                updateRomUpdateHelper.createTestUpdate(room, playerId, description),
                                room
                        )
                                .thenReturn(room))
                .map(room -> apiMapperService.mapToRoomResponse(repositoryMapperService.mapToModel(room), playerId));
    }

    public Mono<GameUpdate> makeMove(@NotNull MakeMoveRequest request) {
        return findById(request.getRoomId())
                .doOnNext(room -> updateRoomProcess(room, request))
                .doOnNext(room -> resultService.checkAndUpdateIfSuccess(room, request))
                .flatMap(room ->
                        produceAndSaveUpdate(
                                updateRomUpdateHelper.createMoveUpdate(
                                        room,
                                        request,
                                        room.getPlayer(request.getPlayerId()).getMarkerType()
                                ),
                                room
                        )
                )
                .doOnNext(update ->
                        log.debug("Move was made in room {} by player {}", update.getId(), update.getPlayerMoveId())
                );
    }

    public Flux<MessageResponse> getStream(
            @NotNull String roomId,
            @NotNull String playerId
    ) {
        return streamReceiver.getStream(roomId, playerFilter(playerId))
                .doFirst(() -> log.debug("New subscriber for the room {} with id {}", roomId, playerId))
                .map(roomUpdate -> apiMapperService.mapToMoveUpdateResponse(roomUpdate, playerId));
    }

    public Flux<MessageResponse> getStream(
            @NotNull String roomId,
            @NotNull String playerId,
            @NotNull String lastRecordId
    ) {
        return streamReceiver.getStream(roomId, lastRecordId, playerFilter(playerId))
                .doFirst(() -> log.debug("New subscriber for the room {} with id {}", roomId, playerId))
                .map(roomUpdate -> apiMapperService.mapToMoveUpdateResponse(roomUpdate, playerId));
    }

    private Predicate<GameUpdate> playerFilter(String playerId) {
//        return roomUpdate -> !Objects.equals(roomUpdate.getPlayerMoveId(), playerId);
        return roomUpdate -> true;
    }

    private void updateRoomIfNeed(RoomHE room, String playerId) {
        if (room.getPlayer(playerId).getStatus() != PlayerStatus.READY) {
            room.getPlayer(playerId).setStatus(PlayerStatus.READY);
            if (room.getNextPlayer(playerId).getStatus() == PlayerStatus.READY) {
                room.setStatus(RoomStatus.PROCESS);
            }
            log.debug("Second player {} was joined to the room {}", playerId, room.getRoomId());
        }
    }

    private void updateRoomProcess(RoomHE room, MakeMoveRequest request) {
        PlayerHE currentPlayer = room.getPlayer(request.getPlayerId());
        PlayerHE nextPlayer = room.getNextPlayer(request.getPlayerId());

        ProcessHE process = room.getProcess();
        process.getField().setMarkerToDot(
                request.getDot().getX(),
                request.getDot().getY(),
                markerMap.get(currentPlayer.getMarkerType())
        );
        process.getField().currentMovesIncrease();
        process.setNextMovePlayerId(nextPlayer.getId());
        process.setLastMoveDate(Instant.now());
    }

    private Mono<GameUpdate> produceAndSaveUpdate(Mono<GameUpdate> roomUpdate, RoomHE room) {
        return roomUpdate
                .flatMap(update -> streamReceiver.addRecord(update)
                        .doOnNext(recordId -> update.setRecordId(recordId.getValue()))
                        .doOnNext(recordId -> postUpdateRoomProcess(room, recordId))
                        .thenReturn(update))
                .doOnNext(any -> roomRepository.save(room));
    }

    private void postUpdateRoomProcess(RoomHE room, RecordId recordId) {
        room.getProcess().setLastRecordId(recordId.getValue());
    }

    private Mono<RoomHE> findById(String id) {
        return Mono.justOrEmpty(roomRepository.findById(id));
    }
}

package com.mozart.tictactoe.service.stream;

import com.mozart.tictactoe.model.redis.RedisDataRecord;
import io.lettuce.core.api.reactive.RedisReactiveCommands;
import java.util.Objects;
import java.util.function.Predicate;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.connection.stream.ReadOffset;
import org.springframework.data.redis.connection.stream.RecordId;
import org.springframework.data.redis.connection.stream.StreamOffset;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.stream.StreamReceiver;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
public abstract class AbstractStreamService<T extends RedisDataRecord> {

    private final ReactiveRedisTemplate<String, String> template;
    private final StreamReceiver<String, ObjectRecord<String, T>> streamReceiver;
    private final RedisReactiveCommands<String, String> commands;

    public Mono<RecordId> addRecord(@NotNull T record) {
        return Mono.justOrEmpty(record)
                .map(this::createRecord)
                .flatMap(r -> template.opsForStream().add(r));
    }

    public Flux<T> getStream(@NotNull String id, @NotNull Predicate<T> filter) {
        return getLatestStream(id)
                .map(this::getRecordWithId)
                .filter(record -> Objects.equals(id, record.getId()))
                .filter(filter);
    }

    public Flux<T> getStream(@NotNull String id, @NotNull String lastRecordId, @NotNull Predicate<T> filter) {
        return getStreamFromRecordId(id, lastRecordId)
                .map(this::getRecordWithId)
                .filter(record -> Objects.equals(id, record.getId()))
                .filter(filter);
    }

    protected abstract String getStreamId(@NotNull String id);

    private T getRecordWithId(ObjectRecord<String, T> record) {
        T redisDataRecord = record.getValue();
        redisDataRecord.setRecordId(record.getId().getValue());
        return redisDataRecord;
    }

    private ObjectRecord<String, T> createRecord(T record) {
        return ObjectRecord.create(getStreamId(record.getId()), record);
    }

    private Flux<ObjectRecord<String, T>> getLatestStream(String id) {
        return streamReceiver.receive(
                StreamOffset.latest(getStreamId(id))
        );
    }

    private Flux<ObjectRecord<String, T>> getStreamFromRecordId(String id, String lastRecordId) {
        return streamReceiver.receive(
                StreamOffset.create(getStreamId(id), ReadOffset.from(lastRecordId))
        );
    }

//    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
//    private static final class AtomicRedisIndex {
//
//        private static final BiFunction<Long, Long, String> FORMAT = (baseId, addId) -> baseId + "-" + addId;
//
//        AtomicLong baseRecordId = new AtomicLong(System.currentTimeMillis());
//        AtomicLong addRecordId = new AtomicLong(0);
//
//        private String getIndex() {
//
//            final Long baseResultRecordId = System.currentTimeMillis();
//
//            if (baseResultRecordId.compareTo(baseRecordId.get()) > 0) {
//                addRecordId.set(0);
//                baseRecordId.set(baseResultRecordId);
//                return FORMAT.apply(baseResultRecordId, 0L);
//            } else {
//                return FORMAT.apply(baseResultRecordId, addRecordId.incrementAndGet());
//            }
//        }
//    }
}

package com.mozart.tictactoe.service.stream;

import com.mozart.tictactoe.model.redis.GameUpdate;
import io.lettuce.core.api.reactive.RedisReactiveCommands;
import java.util.function.Function;
import javax.validation.constraints.NotNull;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.stream.StreamReceiver;
import org.springframework.stereotype.Service;

@Service
public class GameStreamService extends AbstractStreamService<GameUpdate> {

    private static final Function<String, String> ROOM_STREAM_ID = roomId -> "room_stream_" + roomId;

    public GameStreamService(
            ReactiveRedisTemplate<String, String> template,
            StreamReceiver<String, ObjectRecord<String, GameUpdate>> streamReceiver,
            RedisReactiveCommands<String, String> commands
    ) {
        super(template, streamReceiver, commands);
    }

    @Override
    protected String getStreamId(@NotNull String id) {
        return ROOM_STREAM_ID.apply(id);
    }
}

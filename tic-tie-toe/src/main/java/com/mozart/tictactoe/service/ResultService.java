package com.mozart.tictactoe.service;

import com.mozart.tictactoe.model.enums.CellMarker;
import com.mozart.tictactoe.model.enums.RoomStatus;
import com.mozart.tictactoe.model.request.MakeMoveRequest;
import com.mozart.tictactoe.persistance.entity.FieldHE;
import com.mozart.tictactoe.persistance.entity.ProcessHE;
import com.mozart.tictactoe.persistance.entity.ResultHE;
import com.mozart.tictactoe.persistance.entity.RoomHE;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ResultService {

    private final int winLineNumber;

    public ResultService(
            @Value("${application.win-line-number}") int winLineNumber
    ) {
        this.winLineNumber = winLineNumber;
    }

    public void checkAndUpdateIfSuccess(RoomHE room, MakeMoveRequest request) {
        final MakeMoveRequest.Dot dot = request.getDot();

        ProcessHE process = room.getProcess();
        FieldHE field = process.getField();

        final List<List<CellMarker>> cells = field.getParsedCells();
        final int fieldSize = field.getSize();

        final int moveX = dot.getX();
        final int moveY = dot.getY();

        int startCheckX = moveX < winLineNumber ? 0 : moveX - winLineNumber + 1;
        int endCheckX = fieldSize - moveX > winLineNumber ? moveX + winLineNumber - 1 : fieldSize - 1;

        int startCheckY = moveY < winLineNumber ? 0 : moveY - winLineNumber + 1;
        int endCheckY = fieldSize - moveY > winLineNumber ? moveY + winLineNumber - 1 : fieldSize - 1;

        boolean isWin =
                rowWin(cells.get(moveY), startCheckX, endCheckX) ||
                        columnWin(cells, startCheckY, endCheckY, moveX) ||
                        leftDiagWin(cells, startCheckX, endCheckX, startCheckY, endCheckY) ||
                        rightDiagWin(cells, startCheckX, endCheckX, startCheckY, endCheckY);

        if (isWin) {
            process.setResult(
                    ResultHE.win(
                            process,
                            room.getPlayer(request.getPlayerId()).getId(),
                            room.getNextPlayer(request.getPlayerId()).getId()
                    )
            );
            room.setStatus(RoomStatus.END);
        } else if (field.getCurrentMoves() == field.getTotalMoves()) {
            process.setResult(ResultHE.draw(process));
            room.setStatus(RoomStatus.END);
        }
    }

    boolean rowWin(List<CellMarker> cells, int startCheckX, int endCheckX) {
        int numberOfMarkers = 0;
        for (int x = startCheckX; x <= endCheckX; x++) {
            numberOfMarkers = calculateWinMarkerValue(cells.get(x), numberOfMarkers);

            if (Math.abs(numberOfMarkers) == winLineNumber) {
                return true;
            }
        }
        return false;
    }

    boolean columnWin(List<List<CellMarker>> cells, int startCheckY, int endCheckY, int x) {
        int numberOfMarkers = 0;
        for (int y = startCheckY; y <= endCheckY; y++) {
            numberOfMarkers = calculateWinMarkerValue(cells.get(y).get(x), numberOfMarkers);

            if (Math.abs(numberOfMarkers) == winLineNumber) {
                return true;
            }
        }
        return false;
    }

    boolean leftDiagWin(
            List<List<CellMarker>> cells,
            int startCheckX, int endCheckX,
            int startCheckY, int endCheckY
    ) {
        int numberOfMarkers = 0;
        for (int x = startCheckX, y = startCheckY; x <= endCheckX && y <= endCheckY; x++, y++) {
            numberOfMarkers = calculateWinMarkerValue(cells.get(y).get(x), numberOfMarkers);

            if (Math.abs(numberOfMarkers) == winLineNumber) {
                return true;
            }
        }
        return false;
    }

    boolean rightDiagWin(
            List<List<CellMarker>> cells,
            int startCheckX, int endCheckX,
            int startCheckY, int endCheckY
    ) {
        int numberOfMarkers = 0;
        for (int x = startCheckX, y = endCheckY; x <= endCheckX && y >= startCheckY; x++, y--) {
            numberOfMarkers = calculateWinMarkerValue(cells.get(y).get(x), numberOfMarkers);

            if (Math.abs(numberOfMarkers) == winLineNumber) {
                return true;
            }
        }
        return false;
    }

    private int calculateWinMarkerValue(CellMarker marker, int numberOfMarkers) {
        switch (marker) {
            case X:
                return numberOfMarkers > 0 ? numberOfMarkers + 1 : 1;
            case O:
                return numberOfMarkers < 0 ? numberOfMarkers - 1 : -1;
            case N:
            default:
                return 0;
        }
    }
}

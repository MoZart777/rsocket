package com.mozart.tictactoe.service;

import com.mozart.tictactoe.model.response.RoomResponse;
import javax.annotation.PreDestroy;
import javax.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;
import reactor.core.Disposable;
import reactor.core.publisher.FluxProcessor;
import reactor.core.publisher.FluxSink;

@Slf4j
@Component
public class ConnectionLostService {

    private final FluxSink<Pair<String, String>> sink;
    private final RoomService roomService;
    private final Disposable subscription;

    public ConnectionLostService(
            RoomService roomService,
            FluxProcessor<Pair<String, String>, Pair<String, String>> eventProcessor
    ) {
        this.roomService = roomService;
        sink = eventProcessor.sink();

        subscription = eventProcessor
                .flatMap(this::processItem)
                .subscribe();
    }

    public void schedule(@NotNull String roomId, @NotNull String playerId) {
        log.debug("Schedule room {} player {} onto the queue because of the connection was lost", roomId, playerId);
        sink.next(Pair.of(roomId, playerId));
    }

    @PreDestroy
    private void preDestroy() {
        if (subscription != null) {
            subscription.dispose();
        }
    }

    private Publisher<RoomResponse> processItem(Pair<String, String> item) {
        return roomService.leftRoom(item.getLeft(), item.getRight())
                .doFirst(() -> log.debug("Process room {} player {} from the queue", item.getLeft(), item.getRight()));
    }
}

package com.mozart.tictactoe.mapper;

import com.mozart.tictactoe.model.Process;
import com.mozart.tictactoe.model.Room;
import com.mozart.tictactoe.model.redis.GameUpdate;
import com.mozart.tictactoe.model.response.MoveUpdateResponse;
import com.mozart.tictactoe.model.response.RoomInitResponse;
import com.mozart.tictactoe.model.response.RoomResponse;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApiMapperService {

    public final String invitationLink;

    public ApiMapperService(
            @Value("${application.invitation-link}") String invitationLink
    ) {
        this.invitationLink = invitationLink;
    }

    public MoveUpdateResponse mapToMoveUpdateResponse(GameUpdate roomUpdate, String currentPlayerId) {
        return new MoveUpdateResponse(
                roomUpdate.getType(),
                roomUpdate.getDescription(),
                roomUpdate.getRecordId(),
                roomUpdate.getRoomStatus(),
                currentPlayerId.equals(roomUpdate.getNextMovePlayerId()),
                roomUpdate.getPlayerMoveId(),
                roomUpdate.getMarkerType(),
                roomUpdate.getX(),
                roomUpdate.getY(),
                mapToModel(roomUpdate.getResult()),
                roomUpdate.getLastMoveDate()
        );
    }

    public RoomResponse mapToRoomResponse(Room room, String currentPlayerId) {
        String yourId = room.getPlayer(currentPlayerId).getId();
        String enemyId = room.getNextPlayer(currentPlayerId).getId();

        return new RoomResponse(
                room.getId(),
                room.getStatus(),
                room.getPlayer(yourId),
                room.getPlayer(enemyId),
                room.getProcess(),
                Objects.equals(room.getFirstPlayer().getId(), yourId) ?
                        String.format(invitationLink, room.getId(), enemyId) :
                        null
        );
    }

    public RoomInitResponse mapToRoomInitResponse(Room room) {
        return new RoomInitResponse(
                room.getId(),
                room.getFirstPlayer().getId(),
                String.format(invitationLink, room.getId(), room.getFirstPlayer().getId())
        );
    }

    private Process.Result mapToModel(GameUpdate.Result result) {
        return Optional.ofNullable(result)
                .map(r -> new Process.Result(
                        r.getResultType(),
                        r.getWinnerPlayerId(),
                        r.getLoserPlayerId()
                ))
                .orElse(null);
    }
}

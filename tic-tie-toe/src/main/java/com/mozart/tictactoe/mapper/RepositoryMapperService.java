package com.mozart.tictactoe.mapper;

import com.mozart.tictactoe.model.Field;
import com.mozart.tictactoe.model.Player;
import com.mozart.tictactoe.model.Room;
import com.mozart.tictactoe.model.Process;
import com.mozart.tictactoe.persistance.entity.FieldHE;
import com.mozart.tictactoe.persistance.entity.PlayerHE;
import com.mozart.tictactoe.persistance.entity.ProcessHE;
import com.mozart.tictactoe.persistance.entity.ResultHE;
import com.mozart.tictactoe.persistance.entity.RoomHE;
import java.util.Optional;
import javax.validation.constraints.NotNull;
import org.springframework.stereotype.Component;

@Component
public class RepositoryMapperService {

    @NotNull
    public Room mapToModel(@NotNull RoomHE entity) {
        return new Room(
                entity.getRoomId(),
                entity.getStatus(),
                mapToModel(entity.getPlayerById(entity.getFirstPlayerId())),
                mapToModel(entity.getPlayerById(entity.getSecondPlayerId())),
                mapToModel(entity.getProcess())
        );
    }

    public Process mapToModel(ProcessHE entity) {
        return new Process(
                entity.getLastRecordId(),
                entity.getNextMovePlayerId(),
                mapToModel(entity.getField()),
                mapToModel(entity.getResult()),
                entity.getLastMoveDate()
        );
    }

    public Field mapToModel(FieldHE entity) {
        return new Field(
                entity.getParsedCells(),
                entity.getSize(),
                entity.getCurrentMoves()
        );
    }

    public Process.Result mapToModel(ResultHE entity) {
        return Optional.ofNullable(entity)
                .map(r -> new Process.Result(
                        r.getResultType(),
                        r.getWinnerPlayerId(),
                        r.getLoserPlayerId()
                ))
                .orElse(null);
    }

    public Player mapToModel(PlayerHE entity) {
        return new Player(
                entity.getId(),
                entity.getStatus(),
                entity.getName(),
                entity.getMarkerType()
        );
    }
}

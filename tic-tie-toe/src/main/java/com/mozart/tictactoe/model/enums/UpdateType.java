package com.mozart.tictactoe.model.enums;

public enum UpdateType {
    MOVE,
    PLAYER_JOIN,
    PLAYER_LEFT,
    PING,
    TEST
}

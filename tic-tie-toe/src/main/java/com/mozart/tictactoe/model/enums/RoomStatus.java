package com.mozart.tictactoe.model.enums;

public enum RoomStatus {
    WAITED_FOR_PLAYER,
    PROCESS,
    END
}

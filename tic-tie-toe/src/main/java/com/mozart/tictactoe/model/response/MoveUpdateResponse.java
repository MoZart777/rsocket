package com.mozart.tictactoe.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mozart.tictactoe.model.Process;
import com.mozart.tictactoe.model.enums.MarkerType;
import com.mozart.tictactoe.model.enums.RoomStatus;
import com.mozart.tictactoe.model.enums.UpdateType;
import java.time.Instant;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "id", "type", "roomStatus",
        "nextMove", "playerMoveId",
        "markerType", "x", "y",
        "description", "lastMoveDate"
})
public class MoveUpdateResponse extends MessageResponse {

    RoomStatus roomStatus;
    boolean nextMove;

    String playerMoveId;
    MarkerType markerType;
    Integer x;
    Integer y;

    Process.Result result;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    Instant lastMoveDate;

    public MoveUpdateResponse(
            UpdateType type, String description,
            String recordId, RoomStatus roomStatus,
            boolean nextMove, String playerMoveId,
            MarkerType markerType, Integer x, Integer y,
            Process.Result result,
            Instant lastMoveDate
    ) {
        super(recordId, type, description);
        this.roomStatus = roomStatus;
        this.nextMove = nextMove;
        this.playerMoveId = playerMoveId;
        this.markerType = markerType;
        this.x = x;
        this.y = y;
        this.result = result;
        this.lastMoveDate = lastMoveDate;
    }
}

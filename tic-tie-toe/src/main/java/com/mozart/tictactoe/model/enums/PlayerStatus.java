package com.mozart.tictactoe.model.enums;

public enum PlayerStatus {
    READY,
    WAIT
}

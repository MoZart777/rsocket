package com.mozart.tictactoe.model.enums;

public enum MarkerType {
    X, O
}

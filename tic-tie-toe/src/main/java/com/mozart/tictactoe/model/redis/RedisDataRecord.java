package com.mozart.tictactoe.model.redis;

import javax.validation.constraints.NotNull;

public interface RedisDataRecord {

    /**
     * Id use for stream identification
     */
    String getId();

    /**
     * Id use for stream identification
     */
    void setRecordId(@NotNull String recordId);
}

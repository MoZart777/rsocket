package com.mozart.tictactoe.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mozart.tictactoe.model.enums.RoomStatus;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({"id", "status", "firstPlayer", "secondPlayer", "firstMovePlayerId", "process"})
public class Room extends AbstractRoom {

    Player firstPlayer; // always room creator
    Player secondPlayer;

    public Room(String id, RoomStatus status, Player firstPlayer, Player secondPlayer, Process process) {
        super(id, status, process);
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
    }

    @JsonIgnore
    @Override
    public Player getPlayer(@NotNull String playerId) {
        if (playerId.equals(firstPlayer.getId())) {
            return firstPlayer;
        } else if (playerId.equals(secondPlayer.getId())) {
            return secondPlayer;
        } else {
            throw new RuntimeException("Invalid playerId");
        }
    }

    @JsonIgnore
    @Override
    public Player getNextPlayer(@NotNull String playerId) {
        if (playerId.equals(firstPlayer.getId()) && !playerId.equals(secondPlayer.getId())) {
            return secondPlayer;
        } else if (playerId.equals(secondPlayer.getId()) && !playerId.equals(firstPlayer.getId())) {
            return firstPlayer;
        } else {
            throw new RuntimeException("Invalid playerId");
        }
    }
}

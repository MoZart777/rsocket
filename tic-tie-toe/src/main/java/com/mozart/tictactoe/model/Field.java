package com.mozart.tictactoe.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mozart.tictactoe.model.enums.CellMarker;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({"cells", "size", "totalMoves", "currentMoves"})
public class Field implements Serializable {

    // The first list contains Y, the sublist contains X
    @NotNull
    List<List<CellMarker>> cells;
    int size;
    int totalMoves;
    int currentMoves;

    public Field(@NotNull List<List<CellMarker>> cells, int size, int currentMoves) {
        this.cells = new ArrayList<>(cells);
        this.size = size;
        totalMoves = size * size;
        this.currentMoves = currentMoves;
    }

    public void currentMovesIncrease() {
        currentMoves += 1;
    }
}

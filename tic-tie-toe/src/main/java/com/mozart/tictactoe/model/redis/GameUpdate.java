package com.mozart.tictactoe.model.redis;

import com.mozart.tictactoe.model.enums.GameResultType;
import com.mozart.tictactoe.model.enums.MarkerType;
import com.mozart.tictactoe.model.enums.RoomStatus;
import com.mozart.tictactoe.model.enums.UpdateType;
import java.io.Serializable;
import java.time.Instant;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameUpdate implements RedisDataRecord, Serializable {

    String id; // roomId
    String recordId; // redis record ID

    UpdateType type;

    RoomStatus roomStatus;

    String nextMovePlayerId;

    String playerMoveId;
    MarkerType markerType;
    Integer x;
    Integer y;

    String description;

    Result result;

    Instant lastMoveDate;

    @SuppressWarnings({"ParameterNumber", "MethodWithTooManyParameters"})
    public static GameUpdate newMoveUpdate(
            String roomId,
            RoomStatus status,
            String nextMovePlayerId,
            String playerMoveId,
            MarkerType markerType,
            Result result,
            Integer x, Integer y
    ) {
        return new GameUpdate(
                roomId, null,
                UpdateType.MOVE, status,
                nextMovePlayerId,
                playerMoveId, markerType, x, y,
                null, result, Instant.now()
        );
    }

    public static GameUpdate joinUpdate(
            String roomId, RoomStatus status, String nextMovePlayerId, String playerMoveId
    ) {
        return new GameUpdate(
                roomId, null,
                UpdateType.PLAYER_JOIN, status,
                nextMovePlayerId,
                playerMoveId, null, null, null,
                null, null, Instant.now()
        );
    }

    public static GameUpdate playerLeftUpdate(
            String roomId, RoomStatus status, Result result
    ) {
        return new GameUpdate(
                roomId, null,
                UpdateType.PLAYER_LEFT, status,
                null,
                null, null, null, null,
                null, result, Instant.now()
        );
    }

    public static GameUpdate testUpdate(
            String roomId, RoomStatus status, String playerMoveId, String description
    ) {
        return new GameUpdate(
                roomId, null,
                UpdateType.TEST, status,
                null,
                playerMoveId, null, null, null,
                description, null, Instant.now()
        );
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class Result implements Serializable {

        GameResultType resultType;
        String winnerPlayerId;
        String loserPlayerId;
    }
}

package com.mozart.tictactoe.model.enums;

public enum GameResultType {
    WIN,
    DRAW
}

package com.mozart.tictactoe.model.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mozart.tictactoe.model.AbstractRoom;
import com.mozart.tictactoe.model.Player;
import com.mozart.tictactoe.model.Process;
import com.mozart.tictactoe.model.enums.RoomStatus;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(
        {
                "id", "status",
                "yourPlayer", "enemyPlayer",
                "firstMovePlayerId", "process", "links"
        }
)
public class RoomResponse extends AbstractRoom {

    Player yourPlayer;
    Player enemyPlayer;
    Links links;

    @JsonIgnore
    @Override
    public Player getPlayer(@NotNull String playerId) {
        if (playerId.equals(yourPlayer.getId())) {
            return yourPlayer;
        } else if (playerId.equals(enemyPlayer.getId())) {
            return enemyPlayer;
        } else {
            throw new RuntimeException("Invalid playerId");
        }
    }

    @JsonIgnore
    @Override
    public Player getNextPlayer(@NotNull String playerId) {
        if (playerId.equals(yourPlayer.getId()) && !playerId.equals(enemyPlayer.getId())) {
            return enemyPlayer;
        } else if (playerId.equals(enemyPlayer.getId()) && !playerId.equals(yourPlayer.getId())) {
            return yourPlayer;
        } else {
            throw new RuntimeException("Invalid playerId");
        }
    }

    public RoomResponse(
            String id,
            RoomStatus status,
            Player yourPlayer,
            Player enemyPlayer,
            Process process,
            String invitationLink
    ) {
        super(id, status, process);
        this.yourPlayer = yourPlayer;
        this.enemyPlayer = enemyPlayer;
        links = new Links(invitationLink);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPropertyOrder({"invitationLink"})
    public static class Links implements Serializable {
        String invitationLink;
    }
}

package com.mozart.tictactoe.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mozart.tictactoe.model.enums.RoomStatus;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractRoom implements Serializable {

    String id;
    RoomStatus status;

    @NotNull
    Process process;

    public abstract Player getPlayer(@NotNull String playerId);

    public abstract Player getNextPlayer(@NotNull String playerId);
}

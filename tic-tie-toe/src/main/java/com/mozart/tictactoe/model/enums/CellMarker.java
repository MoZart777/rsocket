package com.mozart.tictactoe.model.enums;

public enum CellMarker {
    X, O, N
}

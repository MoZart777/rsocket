package com.mozart.tictactoe.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mozart.tictactoe.model.enums.GameResultType;
import java.io.Serializable;
import java.time.Instant;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({"lastRecordId", "nextMovePlayerId", "field", "result", "lastMoveDate"})
public class Process implements Serializable {

    String lastRecordId;
    String nextMovePlayerId;

    @NotNull
    Field field;

    Result result;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    Instant lastMoveDate;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPropertyOrder({"resultType", "winnerPlayerId", "loserPlayerId"})
    public static class Result implements Serializable {

        GameResultType resultType;
        String winnerPlayerId;
        String loserPlayerId;
    }
}

package com.mozart.tictactoe.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mozart.tictactoe.model.enums.MarkerType;
import java.io.Serializable;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({"firstPlayer", "lastPlayer"})
public class RoomRequest implements Serializable {

    PlayerRequest firstPlayer;
    PlayerRequest secondPlayer;

    @Data
    @NoArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPropertyOrder({"name", "marker"})
    public static class PlayerRequest implements Serializable {
        String name;
        MarkerType markerType;
    }
}

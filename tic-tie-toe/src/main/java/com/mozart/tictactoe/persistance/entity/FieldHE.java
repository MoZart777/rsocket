package com.mozart.tictactoe.persistance.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mozart.tictactoe.model.enums.CellMarker;
import com.mozart.tictactoe.model.enums.MarkerType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = "field")
@FieldDefaults(level = AccessLevel.PRIVATE)
public final class FieldHE extends BaseJsonEntity {

    @Id
    @Column(name = "ROOM_ID", nullable = false, unique = true)
    @Getter
    @Setter
    String roomId;

    @Column(name = "CELLS", nullable = false)
    @Getter
    @Setter
    String cells;

    @Transient
    List<List<CellMarker>> parsedCells;

    @Column(name = "SIZE", nullable = false)
    @Getter
    @Setter
    int size;

    @Column(name = "TOTAL_MOVES", nullable = false)
    @Getter
    @Setter
    int totalMoves;

    @Column(name = "CURRENT_MOVES", nullable = false)
    @Getter
    @Setter
    int currentMoves;

    @OneToOne
    @JoinColumn(name = "ROOM_ID", referencedColumnName = "ROOM_ID")
    @Getter
    @Setter
    ProcessHE process;

    public void setParsedCells(List<List<CellMarker>> parsedCells) {
        this.parsedCells = parsedCells;
        cells = marshal(new CellsJsonHE(parsedCells));
    }

    public List<List<CellMarker>> getParsedCells() {
        if (parsedCells == null) {
            parsedCells = unmarshal(cells, CellsJsonHE.class).getCells();
        }
        return parsedCells;
    }

    public void currentMovesIncrease() {
        currentMoves += 1;
    }

    public void setMarkerToDot(int x, int y, CellMarker cellMarker) {
        getParsedCells().get(y).set(x, cellMarker);
        cells = marshal(new CellsJsonHE(parsedCells));
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CellsJsonHE implements Serializable {
        List<List<CellMarker>> cells = new ArrayList<>();
    }
}

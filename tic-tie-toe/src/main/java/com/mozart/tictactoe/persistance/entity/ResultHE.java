package com.mozart.tictactoe.persistance.entity;

import com.mozart.tictactoe.model.enums.GameResultType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "result")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ResultHE implements Serializable {

    @Id
    @Column(name = "ROOM_ID", nullable = false, unique = true)
    String roomId;

    @Column(name = "RESULT_TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    GameResultType resultType;

    @Column(name = "WINNER_PLAYER_ID")
    String winnerPlayerId;

    @Column(name = "LOSER_PLAYER_ID")
    String loserPlayerId;

    @OneToOne
    @JoinColumn(name = "ROOM_ID", referencedColumnName = "ROOM_ID")
    ProcessHE process;

    public static ResultHE draw(@NotNull ProcessHE process) {
        return new ResultHE(process.getRoomId(), GameResultType.DRAW, null, null, process);
    }

    public static ResultHE win(@NotNull ProcessHE process, String winnerPlayerId, String loserPlayerId) {
        return new ResultHE(process.getRoomId(), GameResultType.WIN, winnerPlayerId, loserPlayerId, process);
    }

}

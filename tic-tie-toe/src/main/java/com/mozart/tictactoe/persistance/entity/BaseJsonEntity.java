package com.mozart.tictactoe.persistance.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.Serializable;
import javax.persistence.MappedSuperclass;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Base Entity for json ability
 *
 * @author <a href="mailto:ikonokhov@wiley.com">Ilya Konokhov</a>
 */
@Data
@EqualsAndHashCode
@MappedSuperclass
public class BaseJsonEntity implements Serializable {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * Marshals given object as JSON string.
     *
     * @param object object to marshal.
     * @return String representation for marshaled object.
     */
    protected String marshal(Serializable object) {
        try {
            return object != null ? MAPPER.writeValueAsString(object) : null;
        } catch (JsonProcessingException ex) {
            throw new IllegalArgumentException("Given object could not be marshalled as JSON string", ex);
        }
    }

    /**
     * Unmarshals given JSON string to given POJO class.
     *
     * @param <T>   type of result.
     * @param json  string to unmarshal.
     * @param clazz class of result.
     * @return unmarshalled JSON object.
     */
    protected <T> T unmarshal(String json, Class<T> clazz) {
        try {
            return json != null ? MAPPER.reader().forType(clazz).readValue(json) : null;
        } catch (IOException ex) {
            throw new IllegalArgumentException("Given JSON string could not be unmarshalled to object", ex);
        }
    }
}

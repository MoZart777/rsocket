package com.mozart.tictactoe.persistance.repository;

import com.mozart.tictactoe.model.AbstractRoom;
import com.mozart.tictactoe.model.Room;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import lombok.Getter;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Qualifier("inMemoryRepository")
public class InMemRoomRepository implements CrudRepository<Room, String> {

    private final Map<String, Room> storage = new ConcurrentHashMap<>();
    @Getter
    private final ReactiveCrudRepository<Room, String> reactiveRoomRepository = new InMemRoomRepositoryReactive();

    @Override
    public <S extends Room> S save(S s) {
        storage.put(s.getId(), s);
        return s;
    }

    @Override
    public <S extends Room> Iterable<S> saveAll(Iterable<S> iterable) {
        iterable.forEach(item -> storage.put(item.getId(), item));
        return iterable;
    }

    @Override
    public Optional<Room> findById(String s) {
        return Optional.ofNullable(storage.get(s));
    }

    @Override
    public boolean existsById(String s) {
        return storage.get(s) != null;
    }

    @Override
    public Iterable<Room> findAll() {
        return storage.values();
    }

    @Override
    public Iterable<Room> findAllById(Iterable<String> iterable) {
        List<Room> result = new ArrayList<>();
        for (String s : iterable) {
            Optional.ofNullable(storage.get(s)).ifPresent(result::add);
        }
        return result;
    }

    @Override
    public long count() {
        return storage.size();
    }

    @Override
    public void deleteById(String s) {
        storage.remove(s);
    }

    @Override
    public void delete(Room room) {
        deleteById(room.getId());
    }

    @Override
    public void deleteAll(Iterable<? extends Room> iterable) {
        for (Room s : iterable) {
            Optional.ofNullable(storage.get(s.getId())).ifPresent(value -> storage.remove(value.getId()));
        }
    }

    @Override
    public void deleteAll() {
        storage.clear();
    }

    private class InMemRoomRepositoryReactive implements ReactiveCrudRepository<Room, String> {

        @Override
        public <S extends Room> Mono<S> save(S s) {
            return Mono.justOrEmpty(s)
                    .doOnNext(value -> storage.put(value.getId(), value));
        }

        @Override
        public <S extends Room> Flux<S> saveAll(Iterable<S> iterable) {
            return Mono.justOrEmpty(iterable)
                    .doOnNext(values -> values.forEach(item -> storage.put(item.getId(), item)))
                    .flatMapIterable(Function.identity());
        }

        @Override
        public <S extends Room> Flux<S> saveAll(Publisher<S> publisher) {
            return null;
        }

        @Override
        public Mono<Room> findById(String s) {
            return Mono.justOrEmpty(storage.get(s));
        }

        @Override
        public Mono<Room> findById(Publisher<String> publisher) {
            return null;
        }

        @Override
        public Mono<Boolean> existsById(String s) {
            return Mono.just(storage.get(s) != null);
        }

        @Override
        public Mono<Boolean> existsById(Publisher<String> publisher) {
            return null;
        }

        @Override
        public Flux<Room> findAll() {
            return Flux.fromIterable(storage.values());
        }

        @Override
        public Flux<Room> findAllById(Iterable<String> iterable) {
            return Flux.fromIterable(iterable)
                    .map(id -> Optional.ofNullable(storage.get(id)))
                    .filter(Optional::isPresent)
                    .map(Optional::get);
        }

        @Override
        public Flux<Room> findAllById(Publisher<String> publisher) {
            return null;
        }

        @Override
        public Mono<Long> count() {
            return Mono.just(storage.size()).map(Long::valueOf);
        }

        @Override
        public Mono<Void> deleteById(String s) {
            return Mono.justOrEmpty(s).doOnNext(storage::remove).then();
        }

        @Override
        public Mono<Void> deleteById(Publisher<String> publisher) {
            return null;
        }

        @Override
        public Mono<Void> delete(Room room) {
            return Mono.justOrEmpty(room).map(AbstractRoom::getId).flatMap(this::deleteById);
        }

        @Override
        public Mono<Void> deleteAll(Iterable<? extends Room> iterable) {
            return Flux.fromIterable(iterable)
                    .map(AbstractRoom::getId)
                    .doOnNext(storage::remove)
                    .then();
        }

        @Override
        public Mono<Void> deleteAll(Publisher<? extends Room> publisher) {
            return null;
        }

        @Override
        public Mono<Void> deleteAll() {
            return Mono.just(storage).doOnNext(Map::clear).then();
        }
    }
}

package com.mozart.tictactoe.persistance.entity;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

@EqualsAndHashCode
@Data
@Entity
@Table(name = "process")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProcessHE implements Serializable {

    @Id
    @Column(name = "ROOM_ID", nullable = false, unique = true)
    String roomId;

    @Column(name = "LAST_RECORD_ID")
    String lastRecordId;

    @Column(name = "NEXT_MOVE_PLAYER_ID")
    String nextMovePlayerId;

    @Column(name = "LAST_MOVE_DATE")
    Instant lastMoveDate;

    @OneToOne(mappedBy = "process", cascade = CascadeType.ALL)
    FieldHE field;

    @OneToOne(mappedBy = "process", cascade = CascadeType.ALL)
    ResultHE result;

    @OneToOne
    @JoinColumn(name = "ROOM_ID", referencedColumnName = "ROOM_ID")
    RoomHE room;
}

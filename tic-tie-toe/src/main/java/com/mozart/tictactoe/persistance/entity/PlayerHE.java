package com.mozart.tictactoe.persistance.entity;

import com.mozart.tictactoe.model.enums.MarkerType;
import com.mozart.tictactoe.model.enums.PlayerStatus;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@EqualsAndHashCode
@NoArgsConstructor
@Data
@Entity
@Table(name = "player")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PlayerHE implements Serializable {

    @Id
    @Column(name = "ID", nullable = false, unique = true)
    String id;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    PlayerStatus status;

    @Column(name = "NAME", nullable = false)
    String name;

    @Column(name = "MARKER_TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    MarkerType markerType;

    @Column(name = "ROOM_ID", nullable = false, insertable = false, updatable = false)
    String roomId;

    @Column(name = "CREATION_DATE", nullable = false)
    Instant creationDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROOM_ID", nullable = false)
    RoomHE room;

    public PlayerHE(String id, PlayerStatus status, String name, MarkerType markerType, RoomHE room) {
        this.id = id;
        this.status = status;
        this.name = name;
        this.markerType = markerType;
        this.roomId = room.getRoomId();
        this.room = room;
    }

    @PrePersist
    public void prePersist() {
        creationDate = Instant.now();
    }
}

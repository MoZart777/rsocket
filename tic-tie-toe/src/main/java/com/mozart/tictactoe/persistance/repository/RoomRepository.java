package com.mozart.tictactoe.persistance.repository;

import com.mozart.tictactoe.persistance.entity.RoomHE;
import org.springframework.data.repository.CrudRepository;

public interface RoomRepository extends CrudRepository<RoomHE, String> {
}

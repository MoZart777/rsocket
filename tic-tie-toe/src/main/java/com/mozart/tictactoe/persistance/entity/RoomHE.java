package com.mozart.tictactoe.persistance.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mozart.tictactoe.model.Player;
import com.mozart.tictactoe.model.enums.RoomStatus;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cascade;

@EqualsAndHashCode
@Data
@Entity
@NoArgsConstructor
@Table(name = "room")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RoomHE implements Serializable {

    @Id
    @Column(name = "ROOM_ID", nullable = false, unique = true)
    String roomId;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    RoomStatus status;

    @Column(name = "FIRST_PLAYER_ID", nullable = false)
    String firstPlayerId;

    @Column(name = "SECOND_PLAYER_ID", nullable = false)
    String secondPlayerId;

    @OneToOne(mappedBy = "room", cascade = CascadeType.ALL)
    ProcessHE process;

    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @OneToMany(mappedBy = "room", orphanRemoval = true, fetch = FetchType.EAGER)
    List<PlayerHE> players = new ArrayList<>();

    public RoomHE(String roomId) {
        this.roomId = roomId;
    }

    @NotNull
    public PlayerHE getPlayerById(@NotNull CharSequence playerId) {
        return players.stream()
                .filter(p -> StringUtils.equals(p.getId(), playerId))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("Player not found"));
    }

    @NotNull
    public PlayerHE getPlayer(@NotNull String playerId) {
        if (playerId.equals(firstPlayerId)) {
            return getPlayerById(firstPlayerId);
        } else if (playerId.equals(secondPlayerId)) {
            return getPlayerById(secondPlayerId);
        } else {
            throw new IllegalArgumentException("Invalid playerId");
        }
    }

    @NotNull
    public PlayerHE getNextPlayer(@NotNull String playerId) {
        if (playerId.equals(firstPlayerId) && !playerId.equals(secondPlayerId)) {
            return getPlayerById(secondPlayerId);
        } else if (playerId.equals(secondPlayerId) && !playerId.equals(firstPlayerId)) {
            return getPlayerById(firstPlayerId);
        } else {
            throw new IllegalArgumentException("Invalid playerId");
        }
    }
}

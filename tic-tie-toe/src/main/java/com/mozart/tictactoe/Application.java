package com.mozart.tictactoe;

import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
@RequiredArgsConstructor
@EnableJpaAuditing
public class Application {

    private final RedisTemplate<String, String> redisTemplate;
    private final RedisConnectionFactory redisConnectionFactory;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @PostConstruct
    private void postConstruct() {
        redisConnectionFactory.getConnection().serverCommands().flushAll();
//        redisTemplate.delete(MESSAGE_STATS_VALUE);
    }
}

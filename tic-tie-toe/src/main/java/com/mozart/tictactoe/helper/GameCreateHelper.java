package com.mozart.tictactoe.helper;

import com.mozart.tictactoe.model.enums.CellMarker;
import com.mozart.tictactoe.model.enums.PlayerStatus;
import com.mozart.tictactoe.model.enums.RoomStatus;
import com.mozart.tictactoe.model.request.RoomRequest;
import com.mozart.tictactoe.persistance.entity.FieldHE;
import com.mozart.tictactoe.persistance.entity.PlayerHE;
import com.mozart.tictactoe.persistance.entity.ProcessHE;
import com.mozart.tictactoe.persistance.entity.RoomHE;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GameCreateHelper {

    private static final Random RANDOM = new Random();

    private final int fieldSize;

    public GameCreateHelper(
            @Value("${application.field-size}") int fieldSize
    ) {
        this.fieldSize = fieldSize;
    }

    public RoomHE createNewRoom(RoomRequest request) {
        final RoomHE room = new RoomHE(UUID.randomUUID().toString());

        PlayerHE firstPlayer = createNewPlayer(request.getFirstPlayer(), room);
        PlayerHE secondPlayer = createNewPlayer(request.getSecondPlayer(), room);

        final Pair<String, String> playerMoves = getPlayersMoveIds(firstPlayer.getId(), secondPlayer.getId());

        room.setStatus(RoomStatus.WAITED_FOR_PLAYER);

        room.setPlayers(Arrays.asList(firstPlayer, secondPlayer));
        room.setFirstPlayerId(playerMoves.getLeft());
        room.setSecondPlayerId(playerMoves.getRight());

        room.setProcess(createNewProcess(room));

        return room;
    }

    private ProcessHE createNewProcess(RoomHE room) {
        ProcessHE process = new ProcessHE();

        process.setRoomId(room.getRoomId());
        process.setRoom(room);
        process.setNextMovePlayerId(room.getFirstPlayerId());
        process.setField(createNewField(process));

        return process;
    }

    private PlayerHE createNewPlayer(RoomRequest.PlayerRequest request, RoomHE room) {
        return new PlayerHE(
                UUID.randomUUID().toString(),
                PlayerStatus.WAIT,
                request.getName(),
                request.getMarkerType(),
                room
        );
    }

    private FieldHE createNewField(ProcessHE process) {
        List<List<CellMarker>> cells = new ArrayList<>(fieldSize);
        for (int x = 0; x < fieldSize; x++) {
            List<CellMarker> row = new ArrayList<>(fieldSize);
            for (int y = 0; y < fieldSize; y++) {
                row.add(CellMarker.N);
            }
            cells.add(row);
        }
        FieldHE field = new FieldHE();
        field.setRoomId(process.getRoomId());
        field.setProcess(process);
        field.setSize(fieldSize);
        field.setTotalMoves(fieldSize * fieldSize);
        field.setCurrentMoves(0);
        field.setParsedCells(cells);

        return field;
    }

    private Pair<String, String> getPlayersMoveIds(String firstPlayerId, String secondPlayerId) {
        return RANDOM.nextInt(2) < 1 ?
                Pair.of(firstPlayerId, secondPlayerId) :
                Pair.of(secondPlayerId, firstPlayerId);
    }
}

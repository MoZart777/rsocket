package com.mozart.tictactoe.helper;

import com.mozart.tictactoe.model.enums.MarkerType;
import com.mozart.tictactoe.model.redis.GameUpdate;
import com.mozart.tictactoe.model.request.MakeMoveRequest;
import com.mozart.tictactoe.persistance.entity.ResultHE;
import com.mozart.tictactoe.persistance.entity.RoomHE;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class UpdateRomUpdateHelper {

    public Mono<GameUpdate> createJoinRoomUpdate(RoomHE room, String playerMoveId) {
        return Mono.just(
                GameUpdate.joinUpdate(
                        room.getRoomId(),
                        room.getStatus(),
                        room.getProcess().getNextMovePlayerId(),
                        playerMoveId
                )
        );
    }

    public Mono<GameUpdate> createPlayerLeftRoomUpdate(RoomHE room) {
        return Mono.just(
                GameUpdate.playerLeftUpdate(
                        room.getRoomId(),
                        room.getStatus(),
                        mapToModel(room.getProcess().getResult())
                )
        );
    }

    public Mono<GameUpdate> createTestUpdate(RoomHE room, String playerMoveId, String description) {
        return Mono.just(
                GameUpdate.testUpdate(
                        room.getRoomId(),
                        room.getStatus(),
                        playerMoveId,
                        description
                )
        );
    }

    public Mono<GameUpdate> createMoveUpdate(RoomHE room, MakeMoveRequest request, MarkerType markerType) {
        return Mono.just(
                GameUpdate.newMoveUpdate(
                        room.getRoomId(),
                        room.getStatus(),
                        room.getProcess().getNextMovePlayerId(),
                        request.getPlayerId(),
                        markerType,
                        mapToModel(room.getProcess().getResult()),
                        Optional.of(request)
                                .map(MakeMoveRequest::getDot)
                                .map(MakeMoveRequest.Dot::getX)
                                .orElse(null),
                        Optional.of(request)
                                .map(MakeMoveRequest::getDot)
                                .map(MakeMoveRequest.Dot::getY)
                                .orElse(null)
                )
        );
    }

    private GameUpdate.Result mapToModel(ResultHE entity) {
        return Optional.ofNullable(entity)
                .map(r -> new GameUpdate.Result(
                        r.getResultType(),
                        r.getWinnerPlayerId(),
                        r.getLoserPlayerId()
                ))
                .orElse(null);
    }
}

package com.mozart.tictactoe.config;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.DirectProcessor;
import reactor.core.publisher.FluxProcessor;

@Configuration
public class BeansConfiguration {

    @Bean
    public FluxProcessor<Pair<String, String>, Pair<String, String>> eventProcessor() {
        return DirectProcessor.create();
    }
}

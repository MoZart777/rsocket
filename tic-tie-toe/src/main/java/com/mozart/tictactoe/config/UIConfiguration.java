package com.mozart.tictactoe.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Configuration
public class UIConfiguration implements BeanFactoryAware {

    private static final String SHORT_PREFIX = "shortBean.";

    private final List<String> foldersPaths;
    private BeanFactory beanFactory;

    public UIConfiguration(@Value("${static.ui.files-path}") List<String> foldersPaths) {
        this.foldersPaths = new ArrayList<>(foldersPaths);
    }

    @Bean
    public RouterFunction<ServerResponse> indexRouter(@Value("classpath:/public/main/index.html") final Resource indexHtml) {
        return route(
                GET("/"),
                request -> ok().contentType(MediaType.TEXT_HTML).bodyValue(indexHtml)
        );
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    @PostConstruct
    protected void configRouterBeans() {
        Assert.state(beanFactory instanceof ConfigurableBeanFactory, "wrong bean factory type");
        ConfigurableBeanFactory configurableBeanFactory = (ConfigurableBeanFactory) beanFactory;

        foldersPaths.forEach(basePath ->
                getResourceFiles(basePath)
                        .forEach(fileName -> {
                            String fullFileName = basePath + "/" + fileName;
                            String shortName = "/" + fileName;

                            Resource resource = new ClassPathResource(fullFileName);

                            configurableBeanFactory.registerSingleton(
                                    fullFileName, getRoute(fullFileName, resource));
                            configurableBeanFactory.registerSingleton(
                                    SHORT_PREFIX + fullFileName, getRoute(shortName, resource));
                        })
        );
    }

    private RouterFunction<ServerResponse> getRoute(String path, Resource resource) {
        return route(
                GET(path),
                request -> ok().contentType(MediaType.TEXT_HTML).bodyValue(resource)
        );
    }

    private List<String> getResourceFiles(String basePath) {
        List<String> filenames = new ArrayList<>();

        try {
            InputStream in = getResourceAsStream(basePath);
            BufferedReader br = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));

            String resource;

            while ((resource = br.readLine()) != null) {
                filenames.add(resource);
            }

            return filenames;
        } catch (IOException e) {
            return filenames;
        }
    }

    private InputStream getResourceAsStream(String resource) {
        final InputStream in = getContextClassLoader().getResourceAsStream(resource);

        return in == null ? getClass().getResourceAsStream(resource) : in;
    }

    private ClassLoader getContextClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }
}
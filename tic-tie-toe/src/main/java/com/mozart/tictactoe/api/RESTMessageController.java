package com.mozart.tictactoe.api;

import com.mozart.tictactoe.model.request.RoomRequest;
import com.mozart.tictactoe.model.response.RoomInitResponse;
import com.mozart.tictactoe.model.response.RoomResponse;
import com.mozart.tictactoe.service.RoomService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
@RestController
public class RESTMessageController {

    private final RoomService roomService;

    @PostMapping(path = "/api/v1/game", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RoomInitResponse> createNewGame(
            @RequestBody RoomRequest request
    ) {
        return roomService.createNewGame(request);
    }

    @GetMapping(path = "/api/v1/game", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RoomResponse> getRoom(
            @RequestParam String roomId,
            @RequestParam String playerId
    ) {
        return roomService.getRoom(roomId, playerId);
    }

    @PostMapping(path = "/api/v1/game/join/{roomId}/{playerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RoomResponse> joinRoom(
            @PathVariable String roomId,
            @PathVariable String playerId
    ) {
        return roomService.joinRoom(roomId, playerId);
    }

    @PostMapping(path = "/api/v1/game/left/{roomId}/{playerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Void> leftRoom(
            @PathVariable String roomId,
            @PathVariable String playerId
    ) {
        return roomService.leftRoom(roomId, playerId).then();
    }

    @PostMapping(path = "/api/v1/game/test/{roomId}/{playerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RoomResponse> testUpdate(
            @PathVariable String roomId,
            @PathVariable String playerId,
            @RequestParam String description
    ) {
        return roomService.testUpdate(roomId, playerId, description);
    }
}

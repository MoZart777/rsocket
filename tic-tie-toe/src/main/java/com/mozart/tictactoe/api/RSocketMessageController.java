package com.mozart.tictactoe.api;

import com.mozart.tictactoe.model.enums.UpdateType;
import com.mozart.tictactoe.model.request.MakeMoveRequest;
import com.mozart.tictactoe.model.request.RoomUpdatesRequest;
import com.mozart.tictactoe.model.response.MessageResponse;
import com.mozart.tictactoe.service.ConnectionLostService;
import com.mozart.tictactoe.service.RoomService;
import java.time.Duration;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
@Controller
public class RSocketMessageController {

    public static final String GET_MESSAGE_ROUTE = "move.updates";
    public static final String FIRE_AND_FORGOT_MESSAGE_ROUTE = "fire-and-forgot.make.move";

    private final RoomService roomService;
    private final ConnectionLostService connectionLostService;

    @MessageMapping(GET_MESSAGE_ROUTE)
    public Flux<MessageResponse> getUpdates(RoomUpdatesRequest request) {
        Flux<MessageResponse> updatesStream = StringUtils.isNotBlank(request.getLastRecordId()) ?
                roomService.getStream(request.getRoomId(), request.getPlayerId(), request.getLastRecordId()) :
                roomService.getStream(request.getRoomId(), request.getPlayerId());

        Flux<MessageResponse> heartbeatStream = getHeartbeatStream(request.getRoomId(), request.getPlayerId());

        return Flux.merge(updatesStream, heartbeatStream);
    }

    @MessageMapping(FIRE_AND_FORGOT_MESSAGE_ROUTE)
    public Mono<Void> makeMove(MakeMoveRequest request) {
        return Mono.justOrEmpty(request)
                .flatMap(r -> roomService.makeMove(request))
                .then();
    }

    private Flux<MessageResponse> getHeartbeatStream(@NotNull String roomId, @NotNull String playerId) {
        return Flux.interval(Duration.ofSeconds(2))
                .map(any -> new MessageResponse(UUID.randomUUID().toString(), UpdateType.PING, "ping-pong"))
                .doFinally(signalType -> {
                    log.debug("Player {} was left the room {}", playerId, roomId);
                    connectionLostService.schedule(roomId, playerId);
                });
    }
}

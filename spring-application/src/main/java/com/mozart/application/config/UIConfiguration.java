package com.mozart.application.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Configuration
public class UIConfiguration {

    @Bean
    public RouterFunction<ServerResponse> indexRouter(@Value("classpath:/public/index.html") final Resource indexHtml) {
        return route(
                GET("/"),
                request -> ok().contentType(MediaType.TEXT_HTML).bodyValue(indexHtml)
        );
    }

    @Bean
    public RouterFunction<ServerResponse> jsRouter(@Value("classpath:/public/app.js") final Resource indexJs) {
        return route(
                GET("/app.js"),
                request -> ok().bodyValue(indexJs)
        );
    }
}
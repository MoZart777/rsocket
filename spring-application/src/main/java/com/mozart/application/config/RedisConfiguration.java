package com.mozart.application.config;

import com.mozart.application.model.Message;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.reactive.RedisReactiveCommands;
import org.springframework.beans.DirectFieldAccessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.stream.StreamReceiver;
import org.springframework.data.redis.stream.StreamReceiver.StreamReceiverOptions;

@Configuration
public class RedisConfiguration {

    public static final String MESSAGE_STATS_VALUE = "message_stats";
    public static final String MESSAGE_STREAM_VALUE = "message_stream";

    @Bean
    public StreamReceiver<String, ObjectRecord<String, Message>> streamReceiver(
            ReactiveRedisConnectionFactory factory) {

        return StreamReceiver
                .create(factory, StreamReceiverOptions
                        .builder()
                        .targetType(Message.class)
                        .build()
                );
    }

    @Bean(destroyMethod = "close")
    public StatefulRedisConnection<String, String> connection(ReactiveRedisConnectionFactory factory) {

        DirectFieldAccessor accessor = new DirectFieldAccessor(factory);
        RedisClient client = (RedisClient) accessor.getPropertyValue("client");

        return client.connect();
    }

    @Bean
    public RedisReactiveCommands<String, String> commands(StatefulRedisConnection<String, String> connection) {
        return connection.reactive();
    }
}

package com.mozart.application;

import java.util.Arrays;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.RedisTemplate;
import static com.mozart.application.config.RedisConfiguration.MESSAGE_STATS_VALUE;
import static com.mozart.application.config.RedisConfiguration.MESSAGE_STREAM_VALUE;

@SpringBootApplication
@RequiredArgsConstructor
public class Application {

    private final RedisTemplate<String, String> redisTemplate;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @PostConstruct
    private void postConstruct() {
        redisTemplate.delete(Arrays.asList(MESSAGE_STATS_VALUE, MESSAGE_STREAM_VALUE));
    }
}

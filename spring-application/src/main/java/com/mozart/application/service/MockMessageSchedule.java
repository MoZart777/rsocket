package com.mozart.application.service;

import com.mozart.application.model.Message;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Configuration
public class MockMessageSchedule {

    private static final Random RANDOM = new SecureRandom();

    private final List<String> messages;
    private final AtomicInteger index = new AtomicInteger(0);
    private final MessageService messageService;

    public MockMessageSchedule(ConfigMessages configMessages, MessageService messageService) {
        List<String> phrases = configMessages.getPhrases();
        messages = new ArrayList<>(phrases);
        this.messageService = messageService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void loadData() {
        Flux.interval(Duration.ofSeconds(1))
                .flatMap(tweet ->
                        messageService.addRMessage(new Message(
                                getRandomCode(),
                                getNextMessage(),
                                Instant.now()
                        )))
                .subscribe();
    }

    private int getRandomCode() {
        return RANDOM.nextInt(5);
    }

    private String getNextMessage() {
        return messages.get(
                index.getAndUpdate(
                        val -> val >= messages.size() - 1 ? 0 : val + 1)
        );
    }

    @Component
    @Data
    @ConfigurationProperties("data")
    @NoArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class ConfigMessages {
        List<String> phrases;
    }
}

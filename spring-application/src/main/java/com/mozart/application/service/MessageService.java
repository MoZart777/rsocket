package com.mozart.application.service;

import com.mozart.application.model.Message;
import com.mozart.application.model.MessageRequest;
import io.lettuce.core.api.reactive.RedisReactiveCommands;
import java.time.Instant;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiFunction;
import javax.annotation.PreDestroy;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.connection.stream.ReadOffset;
import org.springframework.data.redis.connection.stream.Record;
import org.springframework.data.redis.connection.stream.RecordId;
import org.springframework.data.redis.connection.stream.StreamOffset;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.stream.StreamReceiver;
import org.springframework.stereotype.Service;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import static com.mozart.application.config.RedisConfiguration.MESSAGE_STATS_VALUE;
import static com.mozart.application.config.RedisConfiguration.MESSAGE_STREAM_VALUE;

@Slf4j
@Service
public final class MessageService {

    private final ConcurrentMap<Integer, AtomicInteger> idCounter = new ConcurrentHashMap<>();
    private final AtomicRedisIndex indexCounter = new AtomicRedisIndex();

    private final StorageService storage;
    private final ReactiveRedisTemplate<String, String> template;
    private final StreamReceiver<String, ObjectRecord<String, Message>> streamReceiver;
    private final RedisReactiveCommands<String, String> commands;

    private final Disposable subscription;

    public MessageService(
            StorageService storage,
            ReactiveRedisTemplate<String, String> template,
            StreamReceiver<String, ObjectRecord<String, Message>> streamReceiver,
            RedisReactiveCommands<String, String> commands) {

        this.storage = storage;
        this.template = template;
        this.streamReceiver = streamReceiver;
        this.commands = commands;

        Flux<ObjectRecord<String, Message>> featurePollStream = getLatestStream();

        subscription = featurePollStream
                .flatMap(it -> {
                    log.info("Processing message: {}", it);
                    return commands.zaddincr(MESSAGE_STATS_VALUE, 1, it.getValue().getMessage());
                })
                .subscribe();
    }

    public Mono<RecordId> addRMessage(MessageRequest message) {
        return addRMessage(new Message(
                message.getCode(),
                message.getOwnText(),
                Instant.now()
        ));
    }

    public Mono<RecordId> addRMessage(Message message) {
        return Mono.justOrEmpty(createRecord(configIds(message)))
                .flatMap(record -> template.opsForStream().add(record))
                .doOnNext(recordId -> {
                    message.setLastRecordId(recordId.getValue());
                    storage.save(message);
                });
    }

    public Flux<Message> getLatestMessagesStream(@NotNull Integer code, String lastRecordId) {
        return (StringUtils.isBlank(lastRecordId) ? getLatestStream() : getStreamFromId(lastRecordId))
                .map(Record::getValue)
                .filter(message -> Objects.equals(code, message.getCode()));
    }

    private Message configIds(Message message) {
        final Integer code = message.getCode();
        AtomicInteger counter = idCounter.get(code);
        if (counter == null) {
            counter = new AtomicInteger();
            idCounter.put(code, counter);
        }
        message.setId(counter.getAndIncrement());
        message.setLastRecordId(indexCounter.getIndex());
        return message;
    }

    private ObjectRecord<String, Message> createRecord(Message message) {
        return ObjectRecord
                .create(MESSAGE_STREAM_VALUE, message)
                .withId(RecordId.of(message.getLastRecordId()));
    }

    private Flux<ObjectRecord<String, Message>> getLatestStream() {
        return streamReceiver.receive(StreamOffset.latest(MESSAGE_STREAM_VALUE));
    }

    private Flux<ObjectRecord<String, Message>> getStreamFromId(@NotNull String lastRecordId) {
        return streamReceiver.receive(
                StreamOffset.create(
                        MESSAGE_STREAM_VALUE,
                        ReadOffset.from(lastRecordId))
        );
    }

    @PreDestroy
    private void preDestroy() {
        if (subscription != null) {
            subscription.dispose();
        }
    }

    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    private static final class AtomicRedisIndex {

        private static final BiFunction<Long, Long, String> FORMAT = (baseId, addId) -> baseId + "-" + addId;

        AtomicLong baseRecordId = new AtomicLong(System.currentTimeMillis());
        AtomicLong addRecordId = new AtomicLong(0);

        private String getIndex() {

            final Long baseResultRecordId = System.currentTimeMillis();

            if (baseResultRecordId.compareTo(baseRecordId.get()) > 0) {
                addRecordId.set(0);
                baseRecordId.set(baseResultRecordId);
                return FORMAT.apply(baseResultRecordId, 0L);
            } else {
                return FORMAT.apply(baseResultRecordId, addRecordId.incrementAndGet());
            }
        }
    }
}

package com.mozart.application.service;

import com.mozart.application.model.Message;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.validation.constraints.NotNull;
import org.springframework.stereotype.Service;

@Service
public class StorageService {

    private final Map<Integer, Message> storage = new ConcurrentHashMap<>();

    public void save(@NotNull Message message) {
        storage.put(message.getCode(), message);
    }

    public Message findByCode(@NotNull Integer code) {
        return storage.get(code);
    }
}

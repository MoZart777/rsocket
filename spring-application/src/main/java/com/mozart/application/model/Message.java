package com.mozart.application.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.time.Instant;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({"id", "code", "lastRecordId", "message", "date"})
public class Message {

    private static final Pattern COMPILE = Pattern.compile("-", Pattern.LITERAL);

    int id;
    String lastRecordId;
    Integer code;
    String message;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    Instant date;

    public Message(Integer code, String message, Instant date) {
        this.code = code;
        this.message = message;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Message{" +
                "code=" + code +
                ", lastRecordId=" + lastRecordId +
                ", id=" + id +
                ", message=" + message +
                ", date=" + date +
                '}';
    }
}

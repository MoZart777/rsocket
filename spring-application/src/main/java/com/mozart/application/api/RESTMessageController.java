package com.mozart.application.api;

import com.mozart.application.model.Message;
import com.mozart.application.model.MessageRequest;
import com.mozart.application.service.StorageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import static com.mozart.application.api.RSocketMessageController.GET_MESSAGE_ROUTE;

@Slf4j
@RequiredArgsConstructor
@RestController
public class RESTMessageController {

    private final StorageService storageService;
    private final Mono<RSocketRequester> requester;

    @GetMapping(path = "/api/message/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Message getMessageByCode(@PathVariable Integer code) throws InterruptedException {
        Message message = storageService.findByCode(code);
        if (code == 3 || code == 4) {
            log.info("Request process with code = {}. Return record with id = {}. Sleep for 30 sec...",
                    code, message != null ? message.getLastRecordId() : "empty");
            Thread.sleep(30 * 1000);
        }
        if (message != null) {
            log.info("Was send with code={}, id={}, lastRecordId={}",
                    message.getCode(), message.getId(), message.getLastRecordId());
        }
        return message;
    }

    @GetMapping(path = "/api/message/socket/{code}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Message> getSocketMessageByCode(@PathVariable Integer code) {
        return requester.flatMapMany(r -> r
                .route(GET_MESSAGE_ROUTE)
                .data(new MessageRequest(code))
                .retrieveFlux(Message.class)
        );
    }

}

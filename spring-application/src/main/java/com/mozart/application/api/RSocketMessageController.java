package com.mozart.application.api;

import com.mozart.application.model.Message;
import com.mozart.application.model.MessageRequest;
import com.mozart.application.service.MessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
@Controller
public class RSocketMessageController {

    public static final String GET_MESSAGE_ROUTE = "message.by.code";
    public static final String FIRE_AND_FORGOT_MESSAGE_ROUTE = "fire-and-forgot.message.by.code";

    private final MessageService messageService;

    @MessageMapping(GET_MESSAGE_ROUTE)
    public Flux<Message> getMessage(MessageRequest request) {
        return messageService.getLatestMessagesStream(request.getCode(), request.getLastRecordId())
                .doOnNext(message -> log.info("Was send with code={}, id={}, lastRecordId={}",
                        message.getCode(), message.getId(), message.getLastRecordId())
                );
    }

    @MessageMapping(FIRE_AND_FORGOT_MESSAGE_ROUTE)
    public Mono<Void> pushMessage(MessageRequest request) {
        return Mono.justOrEmpty(request)
                .flatMap(r ->
                        StringUtils.isNotBlank(r.getOwnText()) && r.getCode() != null ?
                                messageService.addRMessage(r) :
                                Mono.empty()
                ).then();
    }

}

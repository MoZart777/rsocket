# rsocket-demo
## Server

Start ```Application.java```

RSocket server on port ```7771```

## Client

```bash
cd web && yarn build && yarn serve -s build
```

Web client on http://localhost:5000

Download and install all necessary ng scripts and frameworks  
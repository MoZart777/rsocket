import io.rsocket.Payload;
import io.rsocket.RSocket;
import io.rsocket.core.RSocketClient;
import io.rsocket.core.RSocketConnector;
import io.rsocket.frame.decoder.PayloadDecoder;
import io.rsocket.test.PingClient;
import io.rsocket.transport.netty.client.TcpClientTransport;
import io.rsocket.transport.netty.client.WebsocketClientTransport;
import io.rsocket.util.DefaultPayload;
import java.net.URI;
import java.time.Duration;
import java.util.Random;
import lombok.extern.slf4j.Slf4j;
import org.HdrHistogram.Recorder;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
public class RSocketTest {

    private static final Random RANDOM = new Random();

    @Test
    public void test2() {
        Mono<RSocket> client =
                RSocketConnector.create()
                        .payloadDecoder(PayloadDecoder.ZERO_COPY)
                        .connect(TcpClientTransport.create(7771));

        PingClient pingClient = new PingClient(client);

        Recorder recorder = pingClient.startTracker(Duration.ofSeconds(1));

        int count = 1_000;

        pingClient
                .requestResponsePingPong(count, recorder)
                .doOnTerminate(() -> System.out.println("Sent " + count + " messages."))
                .blockLast();
    }

    @Test
    public void test3() {
        WebsocketClientTransport ws = WebsocketClientTransport.create(URI.create("ws://localhost:7771"));
        RSocket clientRSocket = RSocketConnector.connectWith(ws).block();

        try {
            assert clientRSocket != null;
            // this message have to be returned as: 234
            Flux<Payload> s = clientRSocket.requestChannel(Mono.just(DefaultPayload.create("123")));

            s.take(4).doOnNext(p -> System.out.println(p.getDataUtf8())).blockFirst();
        } finally {
            clientRSocket.dispose();
        }

    }

    @Test
    public void test4() {
        WebsocketClientTransport ws = WebsocketClientTransport.create(URI.create("ws://localhost:7771"));
        Mono<RSocket> source = RSocketConnector.connectWith(ws);

        Flux<Payload> requestChannel = getRequestChannel(source);
        Flux<Payload> requestResponse = getRequestResponse(source);

        Flux.merge(requestChannel, requestResponse).blockLast();
    }

    private Flux<Payload> getRequestChannel(Mono<RSocket> source) {
        return RSocketClient.from(source)
                .requestChannel(
                        Mono.just(RANDOM.nextInt(10000))
                                .map(String::valueOf)
                                .doOnNext(value -> System.out.println("Request: " + value))
                                .map(DefaultPayload::create))
                .doOnSubscribe(s -> System.out.println("Executing RequestChannel..."))
                .doOnNext(payload -> {
                    System.out.println("Response: " + payload.getDataUtf8() + "\n");
                    payload.release();
                    safeSleep(RANDOM.nextInt(10) * 1000);
                })
                .repeat();
    }

    private Flux<Payload> getRequestResponse(Mono<RSocket> source) {
        return RSocketClient.from(source)
                .requestResponse(Mono.just("requestResponse")
                        .doOnNext(value -> System.out.println("Request: " + value))
                        .map(DefaultPayload::create))
                .doOnSubscribe(s -> System.out.println("Executing RequestResponse..."))
                .doOnNext(payload -> {
                    System.out.println("Response: " + payload.getDataUtf8() + "\n");
                    payload.release();
                    safeSleep(RANDOM.nextInt(4) * 1000);
                })
                .repeat();
    }

    private static void safeSleep(int mills) {
        try {
            Thread.sleep(mills);
        } catch (InterruptedException ignored) {
        }
    }
}

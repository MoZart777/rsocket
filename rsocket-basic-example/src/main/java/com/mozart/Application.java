package com.mozart;

import com.mozart.interceptors.AuthSocketInterceptor;
import com.mozart.interceptors.InstrumentedConnectionInterceptor;
import com.mozart.interceptors.InstrumentedSocketInterceptor;
import io.rsocket.Payload;
import io.rsocket.RSocket;
import io.rsocket.core.RSocketServer;
import io.rsocket.transport.netty.server.WebsocketServerTransport;
import io.rsocket.util.DefaultPayload;
import java.util.function.Function;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class Application {

    private static final Function<String, String> VISUALIZATION_FORMATTER =
            item -> item
                    .replace('c', 's')
                    .replace('4', '5')
                    .replace('3', '4')
                    .replace('2', '3')
                    .replace('1', '2');

    public static void main(String[] args) throws InterruptedException {

        final ClientsRepository clients = new ClientsRepository();

        var server = RSocketServer
                .create((setup, sendingSocket) -> Mono.just(new RSocket() {
                    @Override
                    public Mono<Payload> requestResponse(Payload payload) {
                        return Mono.just(payload)
                                .map(any -> "requestResponse")
                                .map(DefaultPayload::create);
                    }

                    @Override
                    public Flux<Payload> requestStream(Payload payload) {
                        return Flux.just(payload)
                                .map(any -> "requestStream")
                                .map(DefaultPayload::create);
                    }

                    @Override
                    public Flux<Payload> requestChannel(Publisher<Payload> payloads) {
                        return Flux.from(payloads)
                                .limitRate(5, 3)
                                .map(Payload::getDataUtf8)
                                .map(VISUALIZATION_FORMATTER)
                                .map(DefaultPayload::create);
                    }

                    @Override
                    public Mono<Void> metadataPush(Payload payload) {
                        return Mono.just(payload)
                                .map(any -> "metadataPush")
                                .then();
                    }
                }))
                .interceptors(registry -> {
                    registry.forResponder(new AuthSocketInterceptor(clients));
                    registry.forResponder(new InstrumentedSocketInterceptor());
                    registry.forConnection(new InstrumentedConnectionInterceptor());
                })
//                .acceptor(SocketAcceptor.forFireAndForget(payload -> {
//                    System.out.println("For fire and forgot");
//                    payload.release();
//                    return Mono.empty();
//                }))
                .bind(WebsocketServerTransport.create(7771))
                .block();
        System.out.println("address " + server.address());
        Thread.currentThread().join();
    }
}

package com.mozart.interceptors;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.rsocket.DuplexConnection;
import io.rsocket.RSocketErrorException;
import io.rsocket.frame.FrameHeaderCodec;
import io.rsocket.frame.FrameType;
import io.rsocket.plugins.DuplexConnectionInterceptor;
import java.net.SocketAddress;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class InstrumentedConnectionInterceptor implements DuplexConnectionInterceptor {

    @Override
    public DuplexConnection apply(Type type, DuplexConnection connection) {
        return new InstrumentedConnection(type, connection);
    }

    private static class InstrumentedConnection implements DuplexConnection {
        private final Type connectionType;
        private final DuplexConnection connection;

        public InstrumentedConnection(Type connectionType, DuplexConnection connection) {
            this.connectionType = connectionType;
            this.connection = connection;
        }

        @Override
        public void sendFrame(int streamId, ByteBuf frame) {
            recordFrame("out", frame, connectionType);
            connection.sendFrame(streamId, frame);
        }

        @Override
        public void sendErrorAndClose(RSocketErrorException errorException) {
            connection.sendErrorAndClose(errorException);
        }

        @Override
        public Flux<ByteBuf> receive() {
            return Flux.from(connection.receive()) //
                    .doOnNext(frame -> recordFrame("in", frame, connectionType));
        }

        @Override
        public double availability() {
            return connection.availability();
        }

        @Override
        public Mono<Void> onClose() {
            return connection.onClose();
        }

        @Override
        public ByteBufAllocator alloc() {
            return connection.alloc();
        }

        @Override
        public SocketAddress remoteAddress() {
            return null;
        }

        @Override
        public void dispose() {
            connection.dispose();
        }

        @Override
        public boolean isDisposed() {
            return connection.isDisposed();
        }

        private void recordFrame(String flux, ByteBuf frame, Type connectionType) {
            FrameType frameType = FrameHeaderCodec.frameType(frame);
            System.out.printf("%s%s:%s%n", "in".equals(flux) ? "->" : "<-", connectionType, frameType);
        }
    }
}

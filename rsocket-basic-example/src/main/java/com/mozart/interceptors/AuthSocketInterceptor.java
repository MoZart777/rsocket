package com.mozart.interceptors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mozart.ClientsRepository;
import io.rsocket.Payload;
import io.rsocket.RSocket;
import io.rsocket.plugins.RSocketInterceptor;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class AuthSocketInterceptor implements RSocketInterceptor {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private final ClientsRepository clientsRepository;

    public AuthSocketInterceptor(ClientsRepository clientsRepository) {
        this.clientsRepository = clientsRepository;
    }

    @Override
    public RSocket apply(RSocket rSocket) {
        return new LoggingRSocket(rSocket, clientsRepository);
    }

    public static class LoggingRSocket implements RSocket {
        private final RSocket rSocket;
        private final ClientsRepository clientsRepository;

        public LoggingRSocket(RSocket rSocket, ClientsRepository clientsRepository) {
            this.rSocket = rSocket;
            this.clientsRepository = clientsRepository;
        }

        @Override
        public Flux<Payload> requestChannel(Publisher<Payload> payloads) {
            var processClient = Flux.from(payloads)
                    .doOnNext(this::authenticateClientIfNeed);

            return rSocket.requestChannel(processClient);
        }

        @Override
        public Mono<Payload> requestResponse(Payload payload) {
            return rSocket.requestResponse(payload);
        }

        @Override
        public Flux<Payload> requestStream(Payload payload) {
            return rSocket.requestStream(payload);
        }

        @Override
        public Mono<Void> metadataPush(Payload payload) {
            return rSocket.metadataPush(payload);
        }

        @Override
        public double availability() {
            return rSocket.availability();
        }

        @Override
        public void dispose() {
            rSocket.dispose();
        }

        @Override
        public boolean isDisposed() {
            return rSocket.isDisposed();
        }

        @Override
        public Mono<Void> onClose() {
            return rSocket.onClose();
        }

        private void authenticateClientIfNeed(Payload p) {
        }
    }
}

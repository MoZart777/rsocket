package com.mozart;

import io.rsocket.RSocket;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Stream;

public class ClientsRepository {

    private final ConcurrentMap<UUID, RSocket> clients = new ConcurrentHashMap<>();

    public void addClient(RSocket rSocket) {
        final UUID clientId = UUID.randomUUID();
        clients.put(clientId, rSocket);
    }

    public void deleteClient(UUID clientId) {
        clients.remove(clientId);
    }

    public RSocket getClient(UUID clientId) {
        return clients.get(clientId);
    }

    public Stream<RSocket> getOtherClients(UUID clientId) {
        return clients.entrySet()
                .parallelStream()
                .filter(set -> set.getKey() != clientId)
                .map(Map.Entry::getValue);
    }
}

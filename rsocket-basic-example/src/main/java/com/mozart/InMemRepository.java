package com.mozart;

import java.util.ArrayList;
import java.util.List;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class InMemRepository {

    private final List<String> storage = new ArrayList<>();

    public void addItem(String item) {
        storage.add(item);
    }

    public void removeItem(String item) {
        storage.remove(item);
    }

    public Mono<String> getTheLastValue() {
        return Flux.fromIterable(storage).last();
    }

    public Flux<String> getValues() {
        return Flux.fromIterable(storage);
    }

}
